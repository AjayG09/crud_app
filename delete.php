<?php 
  require 'config.php';

  // CHECK SESSION
  session_start(); 
  if (!isset($_SESSION['username'])) {
    header('location: index.php');
  }  

  // DELETE ALL PROFILE INFO
  if(isset($_REQUEST['delete_all'])){
    $delete = "DELETE FROM profile WHERE login_id = '".$_SESSION["username"]."'";
    mysqli_query($con, $delete) or die("DELETE QUERY FAILED TO EXECUTE");
    header('location: delete.php');
  }

  // DISPLAY ALL PROFILE INFO
  $sql = "SELECT * FROM profile WHERE login_id = '".$_SESSION["username"]."'";
  $result = mysqli_query($con, $sql) or die("SELECT QUERY FAILED TO EXECUTE");
  $row = mysqli_fetch_assoc($result);


  // DELETE ONLY SELECTED PROFILE INFO
  if(isset($_REQUEST['delete_single'])){
    // echo $_POST['profile'];
    if($_POST['profile'] == $row["full_name"]){
      $_POST["profile"] = null;
      $update =  "UPDATE profile SET full_name = '".$_POST["profile"]."', address = '".$row["address"]."', dob = '".$row["dob"]."' WHERE login_id = '".$_SESSION["username"]."' ";
      var_dump($update);
    } elseif($_POST["profile"] == $row["address"]) {
      $_POST["profile"] = null;
      $update =  "UPDATE profile SET full_name = '".$row["full_name"]."', address = '".$_POST["profile"]."', dob = '".$row["dob"]."' WHERE login_id = '".$_SESSION["username"]."' ";
    } else {
      $_POST["profile"] = "0001-01-01";
      $update =  "UPDATE profile SET full_name = '".$row["full_name"]."', address = '".$row["address"]."', dob = '".$_POST["profile"]."' WHERE login_id = '".$_SESSION["username"]."' ";
    }
    mysqli_query($con, $update) or die("UPDATE QUERY FAILED TO EXECUTE");
    header('location: delete.php');
  }
?>
<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<html lang="en"> 
<head>
  <meta charset="utf-8">
  <title>CRUD | PROFILE</title>
  <!-- 120 word description for SEO purposes goes here. Note: Usage of lang tag. -->
  <meta name="description" lang="en" content="profile">
  <!-- Keywords to help with SEO go here. Note: Usage of lang tag.  -->
  <meta name="keywords" lang="en" content="profile">
  <!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
  <link rel="shortcut icon" href="favicon.ico" />
  <!-- Default style-sheet is for 'media' type screen (color computer display).  -->
  <link rel="stylesheet" media="screen" href="assets/css/style.css" >
  <!-- html5shiv aka html5 shim. Supporting HTML5 and CSS for IE browsers less than IE9. -->	
  <!--[if lt IE 9]>  
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>  
  <![endif]-->
  <!--  FontAwesome  -->
  <link rel="stylesheet" href="assets/vendor/font/fontawesome-all.css">
</head>
<body>
  <!-- header starts here -->
  <header>
  <nav>
    <ul class="cf">
      <li><a href="add.php">Add</a></li>
      <li class="active"><a href="delete.php">Delete</a></li>
      <li><a href="update.php">Update</a></li>
      <li>
        <ul class="logout">
          <li><?php echo $_SESSION['username']; ?></li>
          <li><a href="logout.php">Logout</a></li>
        </ul>
      </li>
    </ul>
  </nav>   
  <!-- header ends here -->
  </header>
  <!-- main starts here -->
  <main>
    <div class="profile">
      <h2>Profile View</h2>
      <div class="profile-form delete-form">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
          <input type="radio" name="profile" id="full_name" value="<?php echo $row['full_name'];?>">
          <label for="full_name">Full Name: <?php echo $row['full_name'];?></label>
          <input type="radio" name="profile" id="address" value="<?php echo $row['address'];?>">
          <label for="address">Address: <?php echo $row['address'];?></label>
          <input type="radio" name="profile" id="dob" value="<?php echo $row['dob'];?>">
          <label for="dob">Date of birth: <?php echo $row['dob'];?></label>
          <button type="submit" name="delete_single" value="delete_single">delete selected record</button>
          <button type="submit" name="delete_all" value="delete_all">delete info</button>
        </form>
      </div>
    </div>
  <!-- main ends here -->  
  </main>
  <script src="assets/vendor/jquery-1.8.3.min.js"></script>
  <script src="assets/js/script.js"></script>
</body>
</html>
