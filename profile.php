<?php 
  require 'config.php';

  session_start(); 
  if (!isset($_SESSION['username'])) {
    header('location: login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
  }

?>
<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<html lang="en"> 
<head>
  <meta charset="utf-8">
  <title>CRUD | PROFILE</title>
  <!-- 120 word description for SEO purposes goes here. Note: Usage of lang tag. -->
  <meta name="description" lang="en" content="profile">
  <!-- Keywords to help with SEO go here. Note: Usage of lang tag.  -->
  <meta name="keywords" lang="en" content="profile">
  <!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
  <link rel="shortcut icon" href="favicon.ico" />
  <!-- Default style-sheet is for 'media' type screen (color computer display).  -->
  <link rel="stylesheet" media="screen" href="assets/css/style.css" >
  <!-- html5shiv aka html5 shim. Supporting HTML5 and CSS for IE browsers less than IE9. -->	
  <!--[if lt IE 9]>  
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>  
  <![endif]-->
  <!--  FontAwesome  -->
  <link rel="stylesheet" href="assets/vendor/font/fontawesome-all.css">
</head>
<body>
  <!-- header starts here -->
  <header>
    <h1>
      <a href="Profile.php" title="Profile">Profile</a>
    </h1>
    <a href="logout.php" title="Logout">Logout</a>   
  <!-- header ends here -->
  </header>
  <!-- main starts here -->
  <main>
    <div class="profile">
      <h2>Profile</h2>
      <div class="profile-form">
        <form>
          <div class="profile-img">
            <img src="assets/images/image.png" alt="profile">
            <input type="file" name="image">
          </div>
          <label for="full_name">Full Name</label>
          <input type="text" name="full_name" value="<?php ?>">
          <label for="address">Address</label>
          <textarea rows="5" cols="20" name="address"><?php ?></textarea>
          <label for="date">Date of birth</label>
          <input type="date" name="dob" value="<?php  ?>">
          <button type="submit" name="submit" value="submit">Update</button>
        </form>
      </div>
    </div>
  <!-- main ends here -->  
  </main>
  <!-- footer starts here -->
  <footer>
  <!-- footer starts here -->  
  </footer>
<script src="assets/vendor/jquery-1.8.3.min.js"></script>
<script src="assets/js/script.js"></script>
</body>
</html>
