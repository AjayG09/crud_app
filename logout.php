<?php
require 'config.php';
session_start();
$_SESSION['username'] = NULL;
unset($_SESSION['username']);
session_destroy();
if ($_SESSION['username'] == "") 
{
	header("Location: index.php");
}
?>