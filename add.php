<?php 
  require 'config.php';

  // CHECK SESSION
  session_start(); 
  if (!isset($_SESSION['username'])) {
    header('location: index.php');
  }
  
  // INSERT PROFILE INFO 
  if($_SERVER['REQUEST_METHOD'] == 'POST'){  
    $sql = "INSERT INTO profile (full_name, address, dob, login_id) VALUES ('".$_POST["full_name"]."', '".$_POST["address"]."', '".$_POST["dob"]."', '".$_SESSION["username"]."')";
    $result = mysqli_query($con, $sql) or die("Failed");
     if ($result) {
        $message = "Added Successfully";
        echo "<script type='text/javascript'>alert('$message');</script>";
     }
  }
?>
<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<html lang="en"> 
<head>
  <meta charset="utf-8">
  <title>CRUD | PROFILE</title>
  <!-- 120 word description for SEO purposes goes here. Note: Usage of lang tag. -->
  <meta name="description" lang="en" content="profile">
  <!-- Keywords to help with SEO go here. Note: Usage of lang tag.  -->
  <meta name="keywords" lang="en" content="profile">
  <!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
  <link rel="shortcut icon" href="favicon.ico" />
  <!-- Default style-sheet is for 'media' type screen (color computer display).  -->
  <link rel="stylesheet" media="screen" href="assets/css/style.css" >
  <!-- html5shiv aka html5 shim. Supporting HTML5 and CSS for IE browsers less than IE9. -->	
  <!--[if lt IE 9]>  
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>  
  <![endif]-->
  <!--  FontAwesome  -->
  <link rel="stylesheet" href="assets/vendor/font/fontawesome-all.css">
</head>
<body>
  <!-- header starts here -->
  <header>
  <nav>
    <ul class="cf">
      <li class="active"><a href="add.php">Add</a></li>
      <li><a href="delete.php">Delete</a></li>
      <li><a href="update.php">Update</a></li>
      <li>
        <ul class="logout">
          <li><?php echo $_SESSION['username']; ?></li>
          <li><a href="logout.php">Logout</a></li>
        </ul>
      </li>
    </ul>
  </nav>   
  <!-- header ends here -->
  </header>
  <!-- main starts here -->
  <main>
    <div class="profile">
      <h2>Profile Add Info</h2>
      <div class="profile-form">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
          <label for="full_name">Full Name</label>
          <input type="text" name="full_name">
          <label for="address">Address</label>
          <textarea rows="5" name="address"></textarea>
          <label for="date">Date of birth</label>
          <input type="date" name="dob">
          <button type="submit" name="submit" value="submit">Add</button>
        </form>
      </div>
    </div>
  <!-- main ends here -->  
  </main>
  <script src="assets/vendor/jquery-1.8.3.min.js"></script>
  <script src="assets/js/script.js"></script>
</body>
</html>
