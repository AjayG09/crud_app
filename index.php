<?php 
require 'config.php';
$login_msg = $username = $password = "";   
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $username = $_POST['username'];
  $password = $_POST['password'];
  if (empty($_POST['username']) && empty($_POST['password'])) { 
    $login_msg = "Enter your username and password";
  } else {
    $username = mysqli_real_escape_string($con, $_POST['username']);
    $password = mysqli_real_escape_string($con, $_POST['password']);
    $password = md5($password);
    $query = "SELECT * FROM login WHERE username='$username' AND password='$password'";
    $results = mysqli_query($con, $query);
    if (mysqli_num_rows($results) == 1) {
      session_start();
      $_SESSION['username'] = $username;
      header('location:add.php');
    }
    else {
      $login_msg = "Invalid Email or Password";
    }
  } 
}

?>
<!doctype html>
<html lang="en"> 
<head>
  <meta charset="utf-8">
  <title>CRUD | Login</title>
  <!-- 120 word description for SEO purposes goes here. Note: Usage of lang tag. -->
  <meta name="description" lang="en" content="crud">
  <!-- Keywords to help with SEO go here. Note: Usage of lang tag.  -->
  <meta name="keywords" lang="en" content="crud login">
  <!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
  <link rel="shortcut icon" href="favicon.ico" />
  <!-- Default style-sheet is for 'media' type screen (color computer display).  -->
  <link rel="stylesheet" media="screen" href="assets/css/style.css" >
  <!-- html5shiv aka html5 shim. Supporting HTML5 and CSS for IE browsers less than IE9. -->  
  <!--[if lt IE 9]>  
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>  
  <![endif]-->
  <!--  FontAwesome  -->
  <link rel="stylesheet" href="assets/vendor/font/fontawesome-all.css">
</head>
<body>
  <div class="container">
    <!-- header starts here -->
    <header>
      <h1>Crud Application</h1>
    <!-- header ends here -->
    </header>
    <!-- main starts here -->
    <main>
      <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
        <label for="username">Username:</label>
        <input type="text" name="username" value="<?php echo $username; ?>">
        <label for="password">Password:</label>
        <input type="password" name="password" value="<?php echo $password; ?>">
        <span class="error"><?php echo $login_msg; ?></span>
        <button type="submit" name="submit" value="submit">Login</button>
      </form>
    <!-- main ends here -->  
    </main>
  </div>
<script src="assets/vendor/jquery-1.8.3.min.js"></script>
<script src="assets/js/script.js"></script>
</body>
</html>
